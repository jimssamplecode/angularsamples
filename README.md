

Steps taken so far:
- Created folder, cd'd
- git init
- echo "/node_modules" >> .gitignore
- commit

Update npm:
- sudo npm cache clean -f
- sudo npm install -g n
- sudo n stable
All a bit broken... now npm is in /usr/local/n/versions/node/14.17.6/bin/npm(and not on classpath)
- /usr/local/n/versions/node/14.17.6/bin/npm install @aws-amplify/cli
Issues:
- upgrade uuid(3.4) -> 7+
- /usr/local/n/versions/node/14.17.6/bin/npm install uuid
- /usr/local/n/versions/node/14.17.6/bin/npm install fsevents (no - mac +15 only)
- /usr/local/n/versions/node/14.17.6/bin/npm audit fix (fails - needs package.json)

New Angular/Amplify project:
- ng new Amplify
- cd Amplify 
- /usr/local/n/versions/node/14.17.6/bin/npm install @aws-amplify/cli@3.17.0
- /usr/local/n/versions/node/14.17.6/bin/npm audit - looks OK (1 moderate DoS)
Commit

Amplify - following https://docs.amplify.aws/cli/start/install/#option-2-follow-the-instructions
- /home/jim/Projects/AngularTests/node_modules/@aws-amplify/cli/bin/amplify configure (path issues again...)

Nope.

Starting again...
- apt purge nodejs ; apt install nodejs
- sudo npm uninstall -g n
- ng new Amplify

Nope.

Trying adding Node's repos: (https://tecadmin.net/install-latest-nodejs-npm-on-debian/)
- wget https://deb.nodesource.com/setup_14.x ; bash setup_14.x
- nodejs (14 - good)
- npm - (pkg) 7.5.2 - bad, installed 6.14 ?? (nodejs repo package contains npm?)
Amplify:
- npm install @aws-amplify/cli - grumbles: uuid 3.3.2, graphql

Nope.

Next day - works through SSH - is this an issue with X?

- amplify configure (worked, added user to IAM eu-west-2)

run amplify init.. does stuff.... OK:
- code: no idea...
AWS: 
- S3 - amplitfy-amplify-dev-.... some files
- CF - nothing




